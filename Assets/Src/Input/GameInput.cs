// GENERATED AUTOMATICALLY FROM 'Assets/Input/GameInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace WAM.Input
{
    public class @GameInput : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @GameInput()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameInput"",
    ""maps"": [
        {
            ""name"": ""PC"",
            ""id"": ""96e6acbd-99c5-47f2-82c4-74303ce30be0"",
            ""actions"": [
                {
                    ""name"": ""MouseMovement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""7ad8286a-a57c-4cf9-951c-8270e52e3865"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MouseClick"",
                    ""type"": ""Value"",
                    ""id"": ""f1b5b6ed-4146-4fd7-9e2b-08cf1e4b9409"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""c4a64fe3-32ff-44c2-9d8d-0054597b6d3e"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3cb1a897-047f-4839-ab1d-8eee8693ef9c"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Device"",
            ""id"": ""fa98492c-7707-4bfb-aa3a-5b3e5afebd7d"",
            ""actions"": [
                {
                    ""name"": ""TouchInput"",
                    ""type"": ""PassThrough"",
                    ""id"": ""04593773-ac3d-44c8-a534-7cfd84a90e09"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TouchPress"",
                    ""type"": ""Button"",
                    ""id"": ""43ffd81a-2d4e-4630-b59f-af3f912e5f79"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TouchPosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""3006ad1e-c3fb-48a5-892e-f4c5ae9d30af"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""519e8123-329d-4977-9ff6-9f3814739251"",
                    ""path"": ""<Touchscreen>/primaryTouch"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TouchInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9746da11-24cf-4440-bc04-40636dd3693d"",
                    ""path"": ""<Touchscreen>/primaryTouch/press"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TouchPress"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bb565f76-ac3e-409a-aea6-0a5d7714c46f"",
                    ""path"": ""<Touchscreen>/primaryTouch/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TouchPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // PC
            m_PC = asset.FindActionMap("PC", throwIfNotFound: true);
            m_PC_MouseMovement = m_PC.FindAction("MouseMovement", throwIfNotFound: true);
            m_PC_MouseClick = m_PC.FindAction("MouseClick", throwIfNotFound: true);
            // Device
            m_Device = asset.FindActionMap("Device", throwIfNotFound: true);
            m_Device_TouchInput = m_Device.FindAction("TouchInput", throwIfNotFound: true);
            m_Device_TouchPress = m_Device.FindAction("TouchPress", throwIfNotFound: true);
            m_Device_TouchPosition = m_Device.FindAction("TouchPosition", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // PC
        private readonly InputActionMap m_PC;
        private IPCActions m_PCActionsCallbackInterface;
        private readonly InputAction m_PC_MouseMovement;
        private readonly InputAction m_PC_MouseClick;
        public struct PCActions
        {
            private @GameInput m_Wrapper;
            public PCActions(@GameInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @MouseMovement => m_Wrapper.m_PC_MouseMovement;
            public InputAction @MouseClick => m_Wrapper.m_PC_MouseClick;
            public InputActionMap Get() { return m_Wrapper.m_PC; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PCActions set) { return set.Get(); }
            public void SetCallbacks(IPCActions instance)
            {
                if (m_Wrapper.m_PCActionsCallbackInterface != null)
                {
                    @MouseMovement.started -= m_Wrapper.m_PCActionsCallbackInterface.OnMouseMovement;
                    @MouseMovement.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnMouseMovement;
                    @MouseMovement.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnMouseMovement;
                    @MouseClick.started -= m_Wrapper.m_PCActionsCallbackInterface.OnMouseClick;
                    @MouseClick.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnMouseClick;
                    @MouseClick.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnMouseClick;
                }
                m_Wrapper.m_PCActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @MouseMovement.started += instance.OnMouseMovement;
                    @MouseMovement.performed += instance.OnMouseMovement;
                    @MouseMovement.canceled += instance.OnMouseMovement;
                    @MouseClick.started += instance.OnMouseClick;
                    @MouseClick.performed += instance.OnMouseClick;
                    @MouseClick.canceled += instance.OnMouseClick;
                }
            }
        }
        public PCActions @PC => new PCActions(this);

        // Device
        private readonly InputActionMap m_Device;
        private IDeviceActions m_DeviceActionsCallbackInterface;
        private readonly InputAction m_Device_TouchInput;
        private readonly InputAction m_Device_TouchPress;
        private readonly InputAction m_Device_TouchPosition;
        public struct DeviceActions
        {
            private @GameInput m_Wrapper;
            public DeviceActions(@GameInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @TouchInput => m_Wrapper.m_Device_TouchInput;
            public InputAction @TouchPress => m_Wrapper.m_Device_TouchPress;
            public InputAction @TouchPosition => m_Wrapper.m_Device_TouchPosition;
            public InputActionMap Get() { return m_Wrapper.m_Device; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(DeviceActions set) { return set.Get(); }
            public void SetCallbacks(IDeviceActions instance)
            {
                if (m_Wrapper.m_DeviceActionsCallbackInterface != null)
                {
                    @TouchInput.started -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchInput;
                    @TouchInput.performed -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchInput;
                    @TouchInput.canceled -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchInput;
                    @TouchPress.started -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchPress;
                    @TouchPress.performed -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchPress;
                    @TouchPress.canceled -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchPress;
                    @TouchPosition.started -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchPosition;
                    @TouchPosition.performed -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchPosition;
                    @TouchPosition.canceled -= m_Wrapper.m_DeviceActionsCallbackInterface.OnTouchPosition;
                }
                m_Wrapper.m_DeviceActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @TouchInput.started += instance.OnTouchInput;
                    @TouchInput.performed += instance.OnTouchInput;
                    @TouchInput.canceled += instance.OnTouchInput;
                    @TouchPress.started += instance.OnTouchPress;
                    @TouchPress.performed += instance.OnTouchPress;
                    @TouchPress.canceled += instance.OnTouchPress;
                    @TouchPosition.started += instance.OnTouchPosition;
                    @TouchPosition.performed += instance.OnTouchPosition;
                    @TouchPosition.canceled += instance.OnTouchPosition;
                }
            }
        }
        public DeviceActions @Device => new DeviceActions(this);
        public interface IPCActions
        {
            void OnMouseMovement(InputAction.CallbackContext context);
            void OnMouseClick(InputAction.CallbackContext context);
        }
        public interface IDeviceActions
        {
            void OnTouchInput(InputAction.CallbackContext context);
            void OnTouchPress(InputAction.CallbackContext context);
            void OnTouchPosition(InputAction.CallbackContext context);
        }
    }
}
