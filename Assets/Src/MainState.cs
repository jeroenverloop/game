﻿using WAM.Util.States;

namespace WAM
{
    public class MainState : StateMachine<MainState.State>
    {
        public MainState(State state) : base(state){}

        public override void Set(State state)
        {
            if (state == CurrentState) return;
            base.Set(state);
        }

        public enum State
        {
            Menu,
            Game,
            ScoreScreen
        }
    }
}
