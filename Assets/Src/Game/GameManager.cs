﻿using UnityEngine;
using WAM.Game.Moles;
using WAM.Game.UI;

namespace WAM.Game
{

    public class GameManager : MonoBehaviour
    {

        [Header("Game")]
        [SerializeField] private GameSettings _gameSettings = null;

        [Header("UI")]
        [SerializeField] private CountDownOverlay _countdownOverlay = null;
        [SerializeField] private ScoreCounter _scoreCounter = null;
        [SerializeField] private GameTimeCounter _gameTimeCounter = null;

        private MoleManager _moleManager;
        private InputManager _inputManager;
        private GameState _gameState;
        private float _gameTime = 0;

        #region UNITY METHODS

        private void Awake()
        {
            _moleManager = new MoleManager(_gameSettings);

            _inputManager = new InputManager();
            _inputManager.OnClicked += OnClicked;

            _gameState = new GameState(GameState.State.Loading);
            _gameState.OnStateChanged += OnGameStateChanged;
            _gameState.Set(GameState.State.Countdown);

            MainManager.PlayerData.OnPlayerScoreUpdated += UpdatePlayerScore;
            MainManager.PlayerData.Reset();

            _gameTimeCounter.UpdateTimer(_gameTime = _gameSettings.GameDuration);
        }

        private void Update()
        {
            switch (_gameState.CurrentState)
            {
                case GameState.State.Playing:
                    _moleManager.Update(Time.deltaTime);
                    _gameTimeCounter.UpdateTimer(_gameTime -= Time.deltaTime);
                    //Go to scorescreen when game timer is up.
                    if(_gameTime <= 0) MainManager.State.Set(MainState.State.ScoreScreen);
                    break;
            }   
        }

        private void OnDestroy()
        {
            _inputManager.OnClicked -= OnClicked;
            _inputManager.EnableInput(false);
            MainManager.PlayerData.OnPlayerScoreUpdated -= UpdatePlayerScore;
        }

        #endregion

        private void OnGameStateChanged(GameState.State from, GameState.State to)
        {
            switch (to)
            {
                // When the game is loaded we generate the moles and start a countdown sequence.
                case GameState.State.Countdown:
                    _moleManager.GenerateMoles();
                    _countdownOverlay.StartCountDown(
                        _gameSettings.CountDownDuration,
                        ()=> {
                            _gameState.Set(GameState.State.Playing);
                        }
                    );
                    break;
                case GameState.State.Playing:
                    _inputManager.EnableInput(true);
                    break;
            }
        }

        private void OnClicked(Vector2 screenPosition)
        {
            Vector2 worldPosition = CameraManager.ScreenToWorldPosition(screenPosition);
            //Check if we find any overlapping mole colliders with the worldposition.
            foreach (MoleBase mole in _moleManager.MoleList)
            {
                if (!mole.Collider.isActiveAndEnabled) continue;
                if (mole.Collider.OverlapPoint(worldPosition))
                {
                    mole.Hit();
                    return;
                }
            }
        }

        private void UpdatePlayerScore(int score)
        {
            _scoreCounter.UpdateScore(score);
        }

        
    }
}
