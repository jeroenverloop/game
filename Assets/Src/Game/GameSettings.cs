﻿using UnityEngine;

namespace WAM.Game
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "WhackAMole/Game/Settings")]
    public class GameSettings : ScriptableObject
    {
        [Tooltip("Duration of 1 game in seconds")]
        [SerializeField] private float _gameDuration = 120;

        [Tooltip("The minimal amount of seconds before a new mole unburrows")]
        [Min(0)]
        [SerializeField] private float _minMoleUnburrowDelay = .5f;
        [Tooltip("The maximum amount of seconds before a new mole unburrows")]
        [Min(0.01f)]
        [SerializeField] private float _maxMoleUnburrowDelay = 2;

        [Tooltip("the minimum distance between moles on the field")]
        [Min(0)]
        [SerializeField] private float _minMoleDistance = 30;

        [Tooltip("CountDown duration before the game starts in seconds")]
        [Min(0)]
        [SerializeField] private int _countDownDuration = 5;

        [Tooltip("An array of spawn settings for specific moles")]
        [SerializeField] private MoleSpawnSettings[] _moleSpawnSettings = null;

        public float GameDuration => _gameDuration;
        public float MinMoleUnburrowDelay => _minMoleUnburrowDelay;
        public float MaxMoleUnburrowDelay => _maxMoleUnburrowDelay;
        public float MinMoleDistance => _minMoleDistance;
        public int CountDownDuration => _countDownDuration;
        public MoleSpawnSettings[] MoleSpawnSettings => _moleSpawnSettings;

        public float RandomUnburrowDelay => Random.Range(MinMoleUnburrowDelay, MaxMoleUnburrowDelay);
    }
}
