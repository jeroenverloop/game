﻿using TMPro;
using UnityEngine;

namespace WAM.Game.UI
{

    public class ScoreCounter : MonoBehaviour
    {

        [SerializeField] private TextMeshProUGUI _scoreText = null;

        public void UpdateScore(int score)
        {
            _scoreText.text = string.Format("Score: {0}", score.ToString());
        }

    }
}
