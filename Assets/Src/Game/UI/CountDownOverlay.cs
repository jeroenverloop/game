﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace WAM.Game
{

    public class CountDownOverlay : MonoBehaviour
    {

        [SerializeField] private TextMeshProUGUI _countdownText = null;

        public void StartCountDown(int duration, Action callback)
        {
            gameObject.SetActive(true);
            StartCoroutine(HandleCountDown(duration, callback));
        }

        //Countdown and invoke callback when ready.
        private IEnumerator HandleCountDown(int duration, Action callback)
        {
            float time = Time.realtimeSinceStartup;
            float timer = duration;
            UpdateTimer(timer);
            while (timer >= 0)
            {
                //Calculate deltaTime because we are in a Coroutine and deltaTime is not reliable.
                float deltaTime = Time.realtimeSinceStartup - time;
                time = Time.realtimeSinceStartup;
                UpdateTimer(timer -= deltaTime);
                yield return null;
            }
            callback?.Invoke();
            gameObject.SetActive(false);
        }

        private void UpdateTimer(float time)
        {
            _countdownText.transform.localScale = Vector3.one * (.7f + (time % 1) * .3f);
            _countdownText.text = Mathf.CeilToInt(time).ToString();
        }

    }
}
