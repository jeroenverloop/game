﻿using System;
using TMPro;
using UnityEngine;

namespace WAM.Game.UI
{

    public class GameTimeCounter : MonoBehaviour
    {

        [SerializeField] private TextMeshProUGUI _timerText = null;

        public void UpdateTimer(float timeLeft)
        {
            //Convert float to a formatted string that represents minutes and seconds.
            _timerText.text = TimeSpan.FromSeconds((double)timeLeft).ToString("mm':'ss");
        }

    }
}
