﻿using Spine.Unity;
using UnityEngine;
using WAM.Util.Audio;

namespace WAM.Game.Moles
{

    public abstract class MoleBase : MonoBehaviour, IMole
    {

        [Header("Settings")]
        [SerializeField] protected MoleSettings _moleSettings = null;

        [Header("Spine")]
        [SerializeField] protected SkeletonAnimation _spineSkeleton = null;

        [Header("Collision")]
        [SerializeField] protected PolygonCollider2D _collider = null;

        public State CurrentState => _state == null ? MoleBase.State.Down : _state.CurrentState;
        public PolygonCollider2D Collider => _collider;

        protected float _unburrowTimer = 0;
        protected MoleState _state;

        public virtual void Init()
        {
            if (_moleSettings == null) throw new System.Exception("Mole has no settings attached");
            if (_spineSkeleton == null) throw new System.Exception("Mole has no SkeletonAnimation attached");
            if (_collider == null) throw new System.Exception("Mole has no collider but needs one for click detection");

            _state = new MoleState(State.Down);
            _state.OnStateChanged += OnMoleStateChanged;

            _spineSkeleton.loop = false;
            _spineSkeleton.timeScale = 0; //Stop the animation until we are ready to start.
            _spineSkeleton.state.Complete += OnAnimationComplete;
        }

        public virtual void Hit()
        {
            //Play MoleHit Audio with randomized pitch for variety
            AudioObject ao = AudioManager.Play("MoleHit");
            ao.Pitch = Random.Range(ao.Pitch - 0.15f, ao.Pitch + 0.15f);
            MainManager.PlayerData.UpdateScore(MainManager.PlayerData.Score + _moleSettings.ScoreOnHit);
            _state.Set(State.Hit);
        }

        public virtual void UnBurrow()
        {
            _state.Set(State.UpAnim);
        }

        protected virtual void Update()
        {
            switch (CurrentState)
            {
                case State.Up:
                    if (_unburrowTimer > 0) _unburrowTimer -= Time.deltaTime;
                    else _state.Set(State.DownAnim);
                    break;
            }
        }

        protected virtual void OnMoleStateChanged(State from, State to)
        {
            switch (to)
            {
                case State.UpAnim:
                    _spineSkeleton.AnimationName = _moleSettings.UpAnimation;
                    _spineSkeleton.timeScale = _moleSettings.UpAnimationSpeed;
                    break;
                case State.DownAnim:
                    _spineSkeleton.AnimationName = _moleSettings.DownAnimation;
                    _spineSkeleton.timeScale = _moleSettings.DownAnimationSpeed;
                    break;
                case State.Hit:
                    _spineSkeleton.AnimationName = _moleSettings.HitAnimation;
                    _spineSkeleton.timeScale = _moleSettings.HitAnimationSpeed;
                    break;
                case State.Up:
                    _unburrowTimer = _moleSettings.RandomUpTime;
                    break;
            }
        }

        /*
         * When a spine animation has reached its last frame this method gets called.
         * According to which animation has ended we will change the state of the mole accordingly
        */
        protected virtual void OnAnimationComplete(Spine.TrackEntry trackEntry)
        {
            if (
                trackEntry.Animation.Name == _moleSettings.UpAnimation &&
                _state.CurrentState != State.Hit
            )
                _state.Set(State.Up);
            else if (
                trackEntry.Animation.Name == _moleSettings.DownAnimation &&
                _state.CurrentState != State.Hit
            )
                _state.Set(State.Down);
            else if (trackEntry.Animation.Name == _moleSettings.HitAnimation)
                _state.Set(State.Down);
        }

        public enum State
        {
            Down,
            DownAnim,
            Up,
            UpAnim,
            Hit
        }
    }
}
