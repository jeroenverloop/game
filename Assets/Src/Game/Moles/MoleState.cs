﻿using WAM.Util.States;

namespace WAM.Game.Moles
{

    public class MoleState : StateMachine<MoleBase.State>
    {
        public MoleState(MoleBase.State state) : base(state){}
    }
}
