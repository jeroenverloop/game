﻿using UnityEngine;

namespace WAM.Game.Moles
{

    public interface IMole
    {

        MoleBase.State CurrentState { get; }

        void UnBurrow();
        void Hit();

    }
}
