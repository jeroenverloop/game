﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace WAM.Game.Moles
{

    public class MoleManager
    {
        private const int MAX_SPAWN_TRIES = 100;

        public List<MoleBase> MoleList { get; private set; }

        private GameSettings _gameSettings;
        private float _moleUnburrowTimer = 0;

        public MoleManager(GameSettings gameSettings)
        {
            _gameSettings = gameSettings;
            _moleUnburrowTimer = _gameSettings.RandomUnburrowDelay;
            MoleList = new List<MoleBase>();
        }


        public void Update(float deltaTime)
        {
            _moleUnburrowTimer -= deltaTime;
            if(_moleUnburrowTimer <= 0)
            {
                UnburrowMole();
                _moleUnburrowTimer += _gameSettings.RandomUnburrowDelay;
            }
        }

        public void GenerateMoles()
        {
            if (MoleList == null) MoleList = new List<MoleBase>();
            //Destroy all moles if there are any left in the list.
            while(MoleList.Count > 0)
            {
                if(MoleList[0] != null) GameObject.Destroy(MoleList[0].gameObject);
                MoleList.RemoveAt(0);
            }

            Rect spawnArea = GetMoleSpawnArea();

            if(_gameSettings.MoleSpawnSettings != null)
            {
                foreach(MoleSpawnSettings spawnSettings in _gameSettings.MoleSpawnSettings)
                {
                    for (int i = 0; i < spawnSettings.Amount; i++)
                    {
                        MoleBase mole = GameObject.Instantiate<MoleBase>(spawnSettings.Prefab);
                        Vector2 spawnLocation = FindSpawnLocation(spawnArea);
                        mole.transform.position = new Vector3(spawnLocation.x, spawnLocation.y, spawnLocation.y / 100f);
                        mole.Init();
                        MoleList.Add(mole);
                    }
                }
            }
        }

        private Vector2 FindSpawnLocation(Rect spawnArea)
        {
            Vector2 spawnLocation = new Vector2(
                Random.Range(spawnArea.x, spawnArea.xMax),
                Random.Range(spawnArea.y, spawnArea.yMax)
            );
            int tries = 0;
            //While the minimum distance of a mole is smaller then the minMoleDistance we keep trying to find a new position.
            while (MinDistToOtherMoles(spawnLocation) < _gameSettings.MinMoleDistance && tries < MAX_SPAWN_TRIES)
            {
                tries++;
                spawnLocation = new Vector2(
                    Random.Range(spawnArea.x, spawnArea.xMax),
                    Random.Range(spawnArea.y, spawnArea.yMax)
                );
            }
            if(tries == MAX_SPAWN_TRIES)
            {
                Debug.LogWarningFormat("Spawn location not found after {0} tries. Spawned random", MAX_SPAWN_TRIES);
            }
            return spawnLocation;
        }

        private Rect GetMoleSpawnArea()
        {
            Rect spawnArea = CameraManager.WorldRect;
            //Slice off some space of the spawnarea to prevent the moles to be half outside of the screen.
            spawnArea.x += 20;
            spawnArea.xMax -= 40;
            spawnArea.y += 20;
            spawnArea.yMax -= 60;
            return spawnArea;
        }

        //Compare the position of all moles to the molePosition and find the closest one.
        private float MinDistToOtherMoles(Vector2 molePosition)
        {
            if (MoleList == null || MoleList.Count == 0) return float.MaxValue;
            float minDist = float.MaxValue;
            foreach(MoleBase mole in MoleList)
            {
                if (mole == null) continue;
                float dist = Vector2.Distance(mole.transform.position, molePosition);
                if (dist < minDist) minDist = dist;
            }
            return minDist;
        }

        private void UnburrowMole()
        {
            //Get an array of moles who are not doing anything aka state == Down
            MoleBase[] burrowedMoles = MoleList.Where(x => x.CurrentState == MoleBase.State.Down).ToArray();
            if(burrowedMoles.Length > 0)
            {
                int moleIndex = Random.Range(0, burrowedMoles.Length);
                burrowedMoles[moleIndex].UnBurrow();
            } 
        }

    }
}
