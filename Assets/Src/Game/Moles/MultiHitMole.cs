﻿namespace WAM.Game.Moles
{
    /// <summary>
    /// This is a small code example on how to extend functionality for more moles in the game.
    /// </summary>
    public class MultiHitMole : MoleBase
    {

        private int _lives = 2;

        public override void Hit()
        {
            _lives--;
            if (_lives > 0) return;
            base.Hit();
        }

    }
}
