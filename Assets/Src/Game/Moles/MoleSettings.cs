﻿using UnityEngine;

namespace WAM.Game.Moles
{

    [CreateAssetMenu(fileName = "MoleSettings", menuName = "WhackAMole/Moles/Settings")]
    public class MoleSettings : ScriptableObject
    {
        [Tooltip("The name of the spine skin used for the mole")]
        [SerializeField] private string _skinName = "default";

        [Tooltip("The name of the unburrowing animation of the mole")]
        [SerializeField] private string _upAnimation = "Up";

        [Tooltip("The time scale of the up animation")]
        [Min(0.01f)]
        [SerializeField] private float _upAnimationSpeed = 1f;

        [Tooltip("The name of the burrowing animation of the mole")]
        [SerializeField] private string _downAnimation = "Down";

        [Tooltip("The time scale of the down animation")]
        [Min(0.01f)]
        [SerializeField] private float _downAnimationSpeed = 1f;

        [Tooltip("The name of the hitting animation of the mole")]
        [SerializeField] private string _hitAnimation = "Hit";

        [Tooltip("The time scale of the hitting animation")]
        [Min(0.01f)]
        [SerializeField] private float _hitAnimationSpeed = 1f;

        [Tooltip("The minimum time a mole spends above ground")]
        [Min(0)]
        [SerializeField] private float _minUnburrowTime = 0;

        [Tooltip("The maximum time a mole spends above ground")]
        [Min(0.01f)]
        [SerializeField] private float _maxUnburrowTime = 1;

        [Tooltip("The amount of score a player will get for hitting this mole")]
        [Min(0)]
        [SerializeField] private int _scoreOnHit = 1;

        public string SkinName => _skinName;
        public string UpAnimation => _upAnimation;
        public float UpAnimationSpeed => _upAnimationSpeed;
        public string DownAnimation => _downAnimation;
        public float DownAnimationSpeed => _downAnimationSpeed;
        public string HitAnimation => _hitAnimation;
        public float HitAnimationSpeed => _hitAnimationSpeed;
        public float MinUnburrowTime => _minUnburrowTime;
        public float MaxUnburrowTime => _maxUnburrowTime;
        public int ScoreOnHit => _scoreOnHit;

        public float RandomUpTime => Random.Range(MinUnburrowTime, MaxUnburrowTime);
    }
}
