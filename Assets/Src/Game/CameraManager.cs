﻿using UnityEngine;
using WAM.Util.Singletons;

namespace WAM.Game
{

    public class CameraManager : MonoSingleton<CameraManager>
    {

        //When _camera is not set we try to find a Camera in the scene.
        public static Camera Camera {
            get {
                if (_instance._camera == null) _instance._camera = GameObject.FindObjectOfType<Camera>();
                return _instance._camera;
            }
        }

        //Translate screen corners to worldposition to define a WorldRect in world units.
        public static Rect WorldRect
        {
            get
            {
                if (Camera == null) return new Rect(0, 0, 0, 0);
                Vector2 worldMin = Camera.ScreenToWorldPoint(new Vector3(Camera.pixelRect.x, Camera.pixelRect.y, 0));
                Vector2 worldMax = Camera.ScreenToWorldPoint(new Vector3(Camera.pixelRect.xMax, Camera.pixelRect.yMax, 0));
                return new Rect(worldMin.x, worldMin.y, worldMax.x - worldMin.x, worldMax.y - worldMin.y);
            }
        }

        public static Vector2 ScreenToWorldPosition(Vector2 screenPosition)
        {
            return Camera.ScreenToWorldPoint(screenPosition);
        }

        [SerializeField] private Camera _camera;


        

    }
}
