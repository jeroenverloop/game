﻿using System;
using UnityEngine;
using WAM.Game.Moles;

namespace WAM.Game
{
    [Serializable]
    public class MoleSpawnSettings
    {
        [Tooltip("Select a mole prefab to be spawned")]
        [SerializeField] private MoleBase _prefab = null;

        [Min(0)]
        [Tooltip("The amount of moles that will be spawned")]
        [SerializeField] private int _amount = 1;

        public MoleBase Prefab => _prefab;
        public int Amount => _amount;
    }
}
