﻿using WAM.Util.States;

namespace WAM
{
    public class GameState : StateMachine<GameState.State>
    {

        public GameState(State state) : base(state){}

        public override void Set(State state)
        {
            if (state == CurrentState) return;
            base.Set(state);
        }

        public enum State
        {
            Loading,
            Countdown,
            Playing
        }
    }
}
