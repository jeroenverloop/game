﻿using System;
using UnityEngine;
using WAM.Input;

namespace WAM.Game
{

    public class InputManager
    {

        public Action<Vector2> OnClicked { get; set; }

        private GameInput _input;

        public InputManager()
        {
            _input = new GameInput();

            _input.PC.MouseClick.performed += ctx => MouseClicked();
            _input.Device.TouchPress.started += ctx => StartTouch();
        }

        public void EnableInput(bool val)
        {
            if (val) _input.Enable();
            else _input.Disable();
        }

        private void MouseClicked()
        {
            OnClicked?.Invoke(_input.PC.MouseMovement.ReadValue<Vector2>());
        }

        private void StartTouch()
        {
            OnClicked?.Invoke(_input.Device.TouchPosition.ReadValue<Vector2>());
        }
    }
}
