﻿using System;

namespace WAM
{
    public class PlayerData
    {

        public Action<int> OnPlayerScoreUpdated { get; set; }

        public int Score { get; private set; }

        public void UpdateScore(int value)
        {
            Score = value;
            OnPlayerScoreUpdated?.Invoke(Score);
        }

        public void Reset() => UpdateScore(0);


    }
}
