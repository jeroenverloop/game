﻿using WAM.Util.Audio;
using WAM.Util.SceneManagement;
using WAM.Util.Serialization;
using WAM.Util.Singletons;

namespace WAM
{
    public class MainManager : StayAliveMonoSingleton<MainManager>
    {
        #region STATIC

        public static MainState State => _instance._stateMachine;
        public static PlayerSaveData PlayerSaveData => _instance._playerSaveData;
        public static PlayerData PlayerData => _instance._playerData;
    
        #endregion

        private MainState _stateMachine = new MainState(MainState.State.Menu);

        private PlayerSaveData _playerSaveData {
            get {
                if (p_playerSaveData == null)
                    p_playerSaveData = LocalDataHandler.Load<PlayerSaveData>("playerData.save");
                return p_playerSaveData;
            }
        }
        private PlayerSaveData p_playerSaveData;


        private PlayerData _playerData {
            get {
                if (p_playerData == null) p_playerData = new PlayerData();
                return p_playerData;
            }
        }
        private PlayerData p_playerData;

        private AudioObject _backgroundMusic;

        private void Awake()
        {
            State.OnStateChanged += OnMainStateChanged;
            /*
             * This background music should propably go to another place but for now i've put it here so the
             * music will always start playing no matter in what scene we start
            */
            if (_backgroundMusic == null) _backgroundMusic = AudioManager.Play("BackgroundMusic", false);
        }

        private void OnMainStateChanged(MainState.State from, MainState.State to)
        {
            switch (to)
            {
                case MainState.State.Menu:
                    SceneLoader.LoadScene("Menu");
                    break;
                case MainState.State.Game:
                    SceneLoader.LoadScene("Game");
                    break;
                case MainState.State.ScoreScreen:
                    SceneLoader.LoadScene("ScoreScreen");
                    break;
            }
        }

    }
}
