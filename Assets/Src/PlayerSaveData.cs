﻿using System;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public class PlayerSaveData : ISerializable
{
    public int HighScore { get; private set; }

    public int SetHighScore(int highScore) => HighScore = highScore;

    public PlayerSaveData()
    {
        HighScore = 0;
    }

    #region SERIALIZATION

    //Retrieve data from the file.
    public PlayerSaveData(SerializationInfo info, StreamingContext stream)
    {
        HighScore = LoadHighScore(info);
    }

    //Serialize data to write it to a file.
    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("HighScore", HighScore);
    }

    private int LoadHighScore(SerializationInfo info)
    {
        try
        {
            return info.GetInt32("HighScore");
        }
        catch (Exception e)
        {
            Debug.LogWarning(e.Message);
            return 0;
        }
    }

    #endregion
}
