﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using WAM.Util.Serialization;

namespace WAM.ScoreScreen
{
    public class ScoreScreenManager : MonoBehaviour
    {

        [SerializeField] private TMPro.TextMeshProUGUI _highScoreText = null;
        [SerializeField] private TMPro.TextMeshProUGUI _scoreText = null;
        [SerializeField] private GameObject _newHighScore = null;

        [SerializeField] private Button _menuButton = null;
        [SerializeField] private Button _againButton = null;

        #region UNITY METHODS

        private void Awake()
        {
            AddButtonListeners();
            HandleScore();
        }

        private void OnDestroy()
        {
            RemoveButtonListeners();
        }

        #endregion

        #region BUTTONS

        private void AddButtonListeners()
        {
            if(_menuButton != null) _menuButton.onClick.AddListener(ToMenuScene);
            else Debug.LogWarning("MenuButton undefined");
            if (_againButton != null) _againButton.onClick.AddListener(ToGameScene);
            else Debug.LogWarning("AgainButton undefined");
        }

        private void RemoveButtonListeners()
        {
            if (_menuButton != null) _menuButton.onClick.RemoveAllListeners();
            else Debug.LogWarning("MenuButton undefined");
            if (_againButton != null) _againButton.onClick.RemoveAllListeners();
            else Debug.LogWarning("AgainButton undefined");
        }

        private void ToMenuScene()
        {
            MainManager.State.Set(MainState.State.Menu);
            SceneManager.LoadScene("Menu");
        }

        private void ToGameScene()
        {
            SceneManager.LoadScene("Game");
        }

        #endregion

        private void HandleScore()
        {
            int highscore = MainManager.PlayerSaveData.HighScore;
            int score = MainManager.PlayerData.Score;

            _highScoreText.text = highscore.ToString();
            _scoreText.text = score.ToString();

            // Save score as highscore when its higher.
            if (score > highscore)
            {
                MainManager.PlayerSaveData.SetHighScore(score);
                LocalDataHandler.Save(MainManager.PlayerSaveData, "playerData.save");
            }

            _newHighScore.SetActive(score > highscore);
        }
    }
}
