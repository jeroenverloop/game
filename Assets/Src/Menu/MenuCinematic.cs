﻿using System.Collections;
using UnityEngine;

namespace WAM.Menu
{
    public class MenuCinematic : MonoBehaviour
    {

        [SerializeField] private GameObject _whack = null;
        [SerializeField] private GameObject _a = null;
        [SerializeField] private GameObject _mole = null;

        public void StartCinematic()
        {
            StartCoroutine(HandleMenuCinematic());
        }

        private IEnumerator HandleMenuCinematic()
        {
            _whack.SetActive(false);
            _a.SetActive(false);
            _mole.SetActive(false);
            yield return new WaitForSeconds(.5f);
            _whack.SetActive(true);
            yield return new WaitForSeconds(1);
            _a.SetActive(true);
            yield return new WaitForSeconds(1);
            _mole.SetActive(true);

        }

    }
}
