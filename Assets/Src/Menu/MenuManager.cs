﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using WAM.Util.SceneManagement;

namespace WAM.Menu
{

    public class MenuManager : MonoBehaviour
    {

        [Header("Buttons")]
        [SerializeField] private Button _quitButton = null;
        [SerializeField] private Button _startGameButton = null;

        [Header("Highscore")]
        [SerializeField] private TextMeshProUGUI _highScoreText = null;

        [Header("Cinematic")]
        [SerializeField] private MenuCinematic _menuCinematic = null;

        #region UNITY METHODS

        private void Awake()
        {
            StartCinematic();
            AddButtonListeners();
            SetHighScoreText();
        }

        private void OnDestroy()
        {
            RemoveButtonListeners();
        }

        #endregion

        #region BUTTONS

        private void AddButtonListeners()
        {
            if (_quitButton != null) _quitButton.onClick.AddListener(QuitGame);
            else Debug.LogWarning("QuitButton undefined");
            if (_startGameButton != null) _startGameButton.onClick.AddListener(StartGame);
            else Debug.LogWarning("StartGameButton undefined");
        }

        private void RemoveButtonListeners()
        {
            if (_startGameButton != null) _startGameButton.onClick.RemoveAllListeners();
            else Debug.LogWarning("StartGameButton undefined");
            if (_quitButton != null) _quitButton.onClick.RemoveAllListeners();
            else Debug.LogWarning("QuitButton undefined");
        }

        private void QuitGame()
        {
            Application.Quit();
        }

        private void StartGame()
        {
            MainManager.State.Set(MainState.State.Game);
        }

        #endregion

        private void SetHighScoreText()
        {
            if(_highScoreText != null) _highScoreText.text = "Highscore: " + MainManager.PlayerSaveData.HighScore;
            else Debug.LogWarning("HighScoreText undefined");
        }

        private void StartCinematic()
        {
            if (_menuCinematic != null) _menuCinematic.StartCinematic();
            else Debug.LogWarning("MenuCinematic undefined");
        }
    }
}