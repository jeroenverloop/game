﻿using System.Collections.Generic;
using UnityEngine;
using WAM.Util.Singletons;

namespace WAM.Util.Audio
{


    public class AudioManager : StayAliveMonoSingleton<AudioManager>
    {

        #region STATIC

        public static AudioLibrary AudioLibrary => _instance._audioLibrary;

        public static float Volume
        {
            get => AudioListener.volume;
            set => AudioListener.volume = value;
        }

        public static AudioObject Get(string assetName, bool managed = true) => _instance.GetAudioObject(assetName, managed);
        public static AudioObject Play(string assetName, bool managed = true) => _instance.PlayAudioObject(assetName, managed);

        #endregion

        [SerializeField] private AudioLibrary _audioLibrary = null;

        public AudioListener AudioListener { get; private set; }

        public List<AudioObject> ManagedAudioObjects
        {
            get
            {
                if (p_managedAudioObjects == null) p_managedAudioObjects = new List<AudioObject>();
                return p_managedAudioObjects;
            }
        }
        private List<AudioObject> p_managedAudioObjects;

        public GameObject AudioContainer
        {
            get
            {
                if (p_audioContainer == null) p_audioContainer = ConstructAudioContainerWithListener();
                return p_audioContainer;
            }
        }
        private GameObject p_audioContainer;


        #region UNITY METHODS

        private void Awake()
        {
            if (_audioLibrary == null) Debug.LogWarning("Add AudioManager to the scene. It needs to have a reference to the AudioLibrary");
        }

        private void Update()
        {
            /*
             * Remove Audio Objects when they are done playing.
             * We can change this for a pooling system when needed.
            */
            for (int i = 0; i < ManagedAudioObjects.Count; i++)
            {
                AudioObject ao = ManagedAudioObjects[i];
                if (!ao.IsPlaying)
                {
                    ManagedAudioObjects.RemoveAt(i);
                    GameObject.DestroyImmediate(ao.Container);
                    i--;
                }
            }
        }

        #endregion

        private AudioObject GetAudioObject(string assetName, bool managed = true)
        {
            /* 
             * Find the asset in the library
             * Create a new AudioObject from the asset
             * When managed add it to the ManagedAudioObjects list
            */
            AudioAsset asset = _audioLibrary[assetName];
            AudioObject ao = new AudioObject(asset, AudioContainer);
            if (asset == null) return null;
            if (managed) ManagedAudioObjects.Add(ao);
            return ao;
        }

        private AudioObject PlayAudioObject(string assetName, bool managed = true)
        {
            AudioObject ao = GetAudioObject(assetName, managed);
            if (ao != null) ao.Play();
            return ao;
        }

        private GameObject ConstructAudioContainerWithListener()
        {
            GameObject go = new GameObject("AUDIO CONTAINER");
            AudioListener = go.AddComponent<AudioListener>();
            GameObject.DontDestroyOnLoad(go);
            Volume = 1f;
            return go;
        }


    }
}
