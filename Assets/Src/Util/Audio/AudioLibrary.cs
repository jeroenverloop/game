﻿using System.Collections.Generic;
using UnityEngine;

namespace WAM.Util.Audio
{

    [CreateAssetMenu(fileName = "AudioLibrary", menuName = "WhackAMole/Audio/Library")]
    public class AudioLibrary : ScriptableObject
    {

        [SerializeField] private AudioAsset[] _audioAssets = null;

        private Dictionary<string, AudioAsset> _audioAssetDictionary;

        public AudioAsset this[string name]
        {
            get
            {
                if (_audioAssetDictionary == null) _audioAssetDictionary = CreateDictionary();
                return FindAsset(name);
            }
        }

        private AudioAsset FindAsset(string name)
        {
            if (_audioAssetDictionary.ContainsKey(name)) return _audioAssetDictionary[name];
            else
            {
                Debug.LogWarningFormat("AudioAsset with name <b>{0}</b> not found", name);
                return null;
            }
        }

        /*
         * Create a dictionary for easy accessible AudioAssets by name
        */
        private Dictionary<string, AudioAsset> CreateDictionary()
        {
            Dictionary<string, AudioAsset> dict = new Dictionary<string, AudioAsset>();
            foreach(AudioAsset asset in _audioAssets) dict[asset.Name] = asset;
            return dict;
        }


    }
}
