﻿
using UnityEngine;

namespace WAM.Util.Audio
{
    public class AudioObject
    {

        public float Volume { get { return _volume; } }
        public float Pitch { get { return _pitch; } set { _pitch = value; } }
        public AudioClip AudioClip { get { return _audioClip; } }
        public bool Loop { get { return _loop; } }
        public bool IsPlaying { get { return Source.isPlaying; } }

        public AudioAsset Asset { get; private set; }
        public GameObject Container { get; private set; }
        public AudioSource Source
        {
            get
            {
                if (p_source == null) p_source = Container.gameObject.AddComponent<AudioSource>();
                return p_source;
            }
        }
        private AudioSource p_source;

        private AudioClip _audioClip
        {
            get { return Source.clip; }
            set { Source.clip = value; }
        }

        private float _volume
        {
            get { return Source.volume; }
            set { Source.volume = value; }
        }

        private float _pitch
        {
            get { return Source.pitch; }
            set { Source.pitch = value; }
        }

        private bool _loop
        {
            get { return Source.loop; }
            set { Source.loop = value; }
        }

        public AudioObject(AudioAsset asset, GameObject audioContainer)
        {
            Asset = asset;
            Container = new GameObject(asset.Name);
            Container.transform.SetParent(audioContainer.transform);

            _audioClip = asset.Clip;
            _volume = asset.Volume;
            _pitch = asset.Pitch;
            _loop = asset.Loop;
        }

        public void Play()
        {
            if (Container == null || Asset == null)
            {
                Debug.LogError("No container or asset defined in AudioObject");
                return;
            }
            Source.Play();
        }

        public void Play(bool loop, AudioClip clip, float volume, float pitch)
        {
            if (Container == null || Asset == null)
            {
                Debug.LogError("No container or asset defined in AudioObject");
                return;
            }
            _volume = volume;
            _audioClip = clip;
            _pitch = pitch;
            _loop = loop;
            Source.Play();
        }

        public void Stop()
        {
            Source.Stop();
        }

    }
}
