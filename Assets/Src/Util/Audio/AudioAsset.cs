﻿using UnityEngine;

namespace WAM.Util.Audio
{

    [CreateAssetMenu(fileName = "AudioAsset" , menuName = "WhackAMole/Audio/Asset")]
    public class AudioAsset : ScriptableObject
    {

        public string Name => _name;
        public AudioClip Clip => _audioClip;
        public float Volume => _volume;
        public float Pitch => _pitch;
        public bool Loop => _loop;

        [SerializeField] private string _name = "";
        [SerializeField] private AudioClip _audioClip = null;
        [SerializeField] private float _volume = 1;
        [SerializeField] private float _pitch = 1;
        [SerializeField] private bool _loop = false;

    }
}
