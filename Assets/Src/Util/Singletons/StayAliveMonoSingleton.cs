﻿using UnityEngine;

namespace WAM.Util.Singletons
{
    public class StayAliveMonoSingleton<T> : MonoBehaviour where T : Component
    {
        protected static T _instance {
            get {
                if(p_instance == null)
                {
                    /*
                     * Try to find an already existing instance of the Singleton.
                     * If any instances are we found the one we are going to use gets selected by the SelectInstanceFromArray Method.
                     * When multiple instances are found we will delete the not selected Instances with the methond RemoveOtherInstances.
                    */
                    T[] instances = GameObject.FindObjectsOfType<T>();
                    if (instances.Length > 0)
                    {
                        p_instance = SelectInstanceFromArray(instances);
                        RemoveOtherInstances(instances, p_instance);
                    }
                    // If we can't find any existing instances we create one from scratch.
                    if (p_instance == null)
                    {
                        GameObject go = new GameObject(typeof(T).Name);
                        p_instance = go.AddComponent<T>();
                    }
                }
                //Keep the monobehaviour alive between scenes.
                DontDestroyOnLoad(p_instance);
                return p_instance;
            }
        }
        private static T p_instance;

        private static T SelectInstanceFromArray(T[] arr)
        {
            if (arr == null || arr.Length == 0) return null;
            return arr[0];
        }

        private static void RemoveOtherInstances(T[] arr, T keepInstance)
        {
            if (arr == null || arr.Length == 0) return;
            foreach (T instance in arr)
            {
                if (instance == keepInstance) continue;
                GameObject.Destroy(instance.gameObject);
            }
        }

    }
}
