﻿using System.Runtime.Serialization;

namespace WAM.Util.Serialization
{
    public interface IDataHandler
    {

        string Path { get; set; }

        T LoadData<T>(string path) where T : ISerializable;
        bool SaveData<T>(T data, string path) where T : ISerializable;
    }
}
