﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace WAM.Util.Serialization
{
    public class LocalDataHandler : IDataHandler
    {
        #region STATIC

        /*
         * This static part is for easily accessible loading and saving without
         * having to create a new instance of the LocalDataHandler everytime.
        */

        public static T Load<T>(string fileName, string path = "") where T : ISerializable
        {
            _instance.Path = path;
            return _instance.LoadData<T>(fileName);
        }

        public static bool Save<T>(T data, string fileName, string path = "") where T : ISerializable
        {
            _instance.Path = path;
            return _instance.SaveData<T>(data, fileName);
        }

        private static LocalDataHandler _instance
        {
            get
            {
                if (p_instance == null) p_instance = new LocalDataHandler();
                return p_instance;
            }
        }
        private static LocalDataHandler p_instance;

        #endregion

        public string Path { get; set; } = "";

        public T LoadData<T>(string fileName) where T : ISerializable
        {
            /*
             * try to find a file by given path and deserialize it into the Generic type.
             * When failed we create a default instance of the given generic type. (Make sure these have an constructor without params to avoid runtime errors)
            */
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                string fullPath = Application.persistentDataPath + "/" + Path + fileName;
                FileStream file = File.Open(fullPath, FileMode.Open);
                T data = (T)bf.Deserialize(file);
                file.Close();
                return data;
            }
            catch(Exception e)
            {
                Debug.LogWarning(e.Message);
                Debug.LogWarning("Creating new " + typeof(T).Name);
                return Activator.CreateInstance<T>();
            }
        }

        public bool SaveData<T>(T data, string fileName) where T : ISerializable
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                string fullPath = Application.persistentDataPath + "/"+ Path + fileName;
                //Make sure our path is reachable by creating all directories specified.
                CreateDirectoriesFromPath(fullPath);

                FileStream file = File.Open(fullPath, FileMode.OpenOrCreate);
                bf.Serialize(file, data);
                file.Close();
                return true;
            }
            catch(Exception e)
            {
                Debug.LogWarning(e.Message);
                return false;
            }
        }

        private void CreateDirectoriesFromPath(string fullPath)
        {
            string[] pathArray = fullPath.Split('/');
            //Strip the last part of the path from the string because this is the filename.
            string directoryPath = String.Join("/", pathArray, 0, pathArray.Length - 1);
            Directory.CreateDirectory(directoryPath);
        }

    }
}
