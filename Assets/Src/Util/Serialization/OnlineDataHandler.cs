﻿using System;
using System.Runtime.Serialization;

namespace WAM.Util.Serialization
{

    public class DatabaseDataHandler : IDataHandler
    {

        #region STATIC

        public static T Load<T>(string fileName, string path = "") where T : ISerializable
        {
            _instance.Path = path;
            return _instance.LoadData<T>(fileName);
        }

        public static bool Save<T>(T data, string fileName, string path = "") where T : ISerializable
        {
            _instance.Path = path;
            return _instance.SaveData<T>(data, fileName);
        }

        private static LocalDataHandler _instance
        {
            get
            {
                if (p_instance == null) p_instance = new LocalDataHandler();
                return p_instance;
            }
        }
        private static LocalDataHandler p_instance;

        #endregion

        public string Path { get; set; } = "";

        public T LoadData<T>(string path) where T : ISerializable
        {
            throw new Exception("Loading data from database not yet implemented");
        }

        public bool SaveData<T>(T data, string path) where T : ISerializable
        {
            throw new Exception("Saving data to database not yet implemented");
        }

    }
}
