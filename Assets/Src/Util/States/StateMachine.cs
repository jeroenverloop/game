﻿using System;

namespace WAM.Util.States
{
    public abstract class StateMachine<T> where T : struct, IConvertible, IComparable
    {

        public Action<T, T> OnStateChanged { get; set; }

        public T PreviousState { get; protected set; }
        public T CurrentState { get; protected set; }

        public StateMachine(T startState)
        {
            if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type");
            PreviousState = startState;
            CurrentState = startState;
        }

        public virtual void Set(T state)
        {
            PreviousState = CurrentState;
            CurrentState = state;
            OnStateChanged?.Invoke(PreviousState, CurrentState);
        }

    }
}
