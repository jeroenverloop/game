﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using WAM.Util.Singletons;

namespace WAM.Util.SceneManagement
{
    /*
     * The reason this class is a MonoBehaviour is for the usage of StartCoroutine
    */
    public class SceneLoader : StayAliveMonoSingleton<SceneLoader>
    {

        #region STATIC

        public static Action<string> OnSceneLoad { get; set; }

        public static void LoadScene(string name, Action callback = null) => _instance.LoadSceneOnInstance(name, callback);

        #endregion

        private void LoadSceneOnInstance(string name, Action callback)
        {
            StartCoroutine(HandleLoadScene(name, callback));
        }

        private IEnumerator HandleLoadScene(string sceneName, Action callback)
        {
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);

            while (!asyncOperation.isDone) yield return null;

            callback?.Invoke();
            OnSceneLoad?.Invoke(sceneName);
        }

    }
}
