## Whack A Mole

This is a small android game prototype made for **GameBasics** as a job application project. **Lets Whack some moles!**

**Note** this prototype has been build in Unity 2020.1.0a20

---

## Explanation

I've set up this prototype for scalability and to use as a showcase of my knowledge on how to build bigger projects.
Most of the systems are a bit "overdone" for the scale of this prototype at this stage.

---

## Tips

1. Under the folder Settings you can find some basic settings for the game and for the 2 different moles
2. Under Audio there is a AudioLibrary where you can change Audio Assets for use in the game.
3. For now the highscore is saved by Serializing data into a file. This data gets deserialized into an object inherited by ISerializable for full control of serializing and deserializing the data.
4. Moles can be easily extended. I've added a simple example: MultiHitMole.cs. Due to not having any visual feedback in the game i've not added this mole type into the game.
5. Under the folder Src/Input there is a file GameInput.cs. This file is automaticly added by the new Input system. (I did not write this code)
6. See the **Builds** folder for that latest Android build. (I included this in Git for the simplicity of sharing)